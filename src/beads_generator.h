#ifndef BEADS_GENERATOR
#define BEADS_GENERATOR

#include <stdint>
#include <vector>
#include <string>

#include <SFML/Graphics/Image.hpp>

namespace kp
{
    std::vector<std::vector<char*>> createBeadsLayout(int32_t columns, int32_t rows, sf::Image& model) const;
}

#endif // !BEADS_GENERATOR
